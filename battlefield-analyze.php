<!DOCTYPE html>
<head>
<title>Battelfield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 <h1>Battelfield Analysis</h1>
 
 <h2> Latest Crituques</h2>
 
 <?php
 
stmt = $mysqli->prepare("select ammunition, soldiers, duration, critique from reports order by timestamps LIMIT 5");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->execute();
 
$stmt->bind_result($myAmmo, $mySoldiers, $myDuration, $myCritique);

echo "<ul>\n";
while($stmt->fetch()){
	printf("\t<li>%s %s</li>\n",
		htmlspecialchars($myAmmo),
		htmlspecialchars($mySoldiers),
                htmlspecialchars($myDuration),
                htmlspecialchars($myCritique)
	);
}
echo "</ul>\n";

 
 ?>
<!-- CONTENT HERE -->
 
 <h2>Battle Statistics</h2>
 
</div></body>
</html>