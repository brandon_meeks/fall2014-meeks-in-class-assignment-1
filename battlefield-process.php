<!DOCTYPE html>

<body>
<?php

$mysqli = new mysqli('localhost', 'root', 'grifdog', 'battlefield');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}


if (isset($_POST['ammo'] )&&
	isset($_POST['soldiers'])&&
	isset($_POST['duration'])&&
	isset($_POST['critique'] )) {
	
	$ammo =     (int) $_POST['ammo'];
	$soldiers = (int) $_POST['soldiers'];
	$duration = (int) $_POST['duration'];
	$critique = (string) $_POST['critique'];

	echo $ammo, $soldiers, $duration, $critique;
	
	$stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique)
				 values (?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	
	$stmt->bind_param('iids', $ammo, $soldiers, $duration, $critique);
 
	$stmt->execute();
	 
	$stmt->close();
		
	

	
}

else{

	echo "Not All Variables Present";

 
};
?>
</body>
</html>
